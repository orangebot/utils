#!/bin/sh

if test -z "$1"; then
	warn USAGE: $0 query '[files...]'
fi

query="$1"
shift

if [ $# -ge 1 ]; then
	grep -n -i "$query" "$@"
else
	grep -n -i "$query" *
fi

