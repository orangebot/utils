DEST = $(HOME)/bin

BINS = \
askpass \
at \
cpdir \
dev-notify \
ducks \
dwmcmd \
f \
genpass \
goto \
i+ \
i- \
lsr \
mntiso \
night \
nqdl \
pidof \
pinfo \
plumbsel \
pw \
pwait \
roll \
rot \
rot-find \
rotate \
setperm \
snippit \
snippit \
ss \
stab \
todo \
uni \
upscale \
vol+ \
vol- \
waitfor \
warn \
xbps-gui \
xidle

INST = $(BINS:%=$(DEST)/%)

CLEAN= \
rot rot.o \
warn warn.o \
vol- vol+

all: $(BINS)

opt:
	@echo 'CC =' $(CC)
	@echo 'DEST =' $(DEST)
	@echo 'BINS =' $(BINS)
	@echo 'INST =' $(INST)
	@echo 'CLEAN =' $(CLEAN)

install: $(INST)

$(DEST)/%: %
	install -m 750 $< $@

clean:
	rm -f $(CLEAN)

rot: rot.o
	$(CC) -o $@ $< $(LDFLAGS)

warn: warn.o
	$(CC) -o $@ $< $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

vol-: vol.sh
	sed s/SIGN/-/ <vol.sh > vol-

vol+: vol.sh
	sed s/SIGN/+/ <vol.sh > vol+

