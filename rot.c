#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_SIZE 8192

int main(int argc, char *argv[]) {
	/* Check that we have a rot value. */
	if(argc != 2){
		fprintf(stderr, "Error: expected 1 arg, got %d.\n", argc - 1);
		return 1;
	}
	
	/* Set asside some memory for input. */
	char buf[BUF_SIZE];
	
	/* Make sure the rot value is valid. */
	int rot = atoi(argv[1]);
	if (rot < 1) {
		fprintf(stderr, "Error: input is an invalid number.\n");
		return 2;
	}
	
	int chars_read = 0;
	do {
		/* Read standard input into the buffer. */
		chars_read = fread(buf, sizeof(char), BUF_SIZE, stdin);
		
		for (int i = 0; i != chars_read; ++i) {
			if (buf[i] > 64 && buf[i] < 91) {
				buf[i] = ((buf[i] - 65 + rot) % 26) + 65;
			}
			else if (buf[i] > 96 && buf[i] < 123) {
				buf[i] = ((buf[i] - 97 + rot) % 26) + 97;
			}
		}
		
		/* Write the buffer to standard out. */
		fwrite(buf, sizeof(char), chars_read, stdout);
	} while (chars_read == BUF_SIZE);

	return 0;
}
