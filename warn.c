/*
Nicola Quoziente
Basically 'echo', but to stderr.
*/

#include <stdio.h>

int main(int argc, char** argv) {
	for (int i = 1; i < argc; ++i) {
		fputs(argv[i], stderr);
		if (i != (argc - 1)) {
			fputs(" ", stderr);
		}
	}
	fputs("\n", stderr);
	return 0;
}
