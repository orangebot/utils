#!/bin/sh

usage () {
	echo usage: $0 '[--help] [-n LEVEL] [-s SEGMENT_SECONDS] [-h HEIGHT] [-t TMPDIR] [-o OUTPUT] INPUT';
}

vidinfo () {
	ffprobe -show_streams -select_streams 0 "$in" 2> /dev/null \
		| awk -F '=' '$1 == "'$1'" {print $2}';
}

#Define defaults
tmp=/tmp/upscale
seg=120
ratio=2
noise=1


#Process args
if [ $# -eq 0 ]; then usage; fi

while [ $# -gt 0 ]; do
	case $1 in
	'--help')
		usage
		exit
		;;
	'-t')
		shift
		tmp=$1
		;;
	'-n')
		shift
		noise=$1
		;;
	'-s')
		shift
		seg=$1
		;;
	'-h')
		shift
		dheight=$1
		;;
	'-o')
		shift
		out=$1
		;;
	*)
		in=$1
		;;
	esac
	shift
done

if [ -z "$in" ]; then
	warn Error: need input file
	exit 1
fi

if [ -z "$out" ]; then
	d=`dirname "$in"`
	f=`basename "$in"`
	out="$d/upscale-$f"
fi


#Get info about the input file
fps=`vidinfo avg_frame_rate`
sar=`vidinfo sample_aspect_ratio`
dar=`vidinfo display_aspect_ratio`
oheight=`vidinfo height`
fmt=`echo "$out" | awk -F '.' '{print $NF}'`

if [ -n "$dheight" ]; then
	ratio=`echo $dheight '/' $oheight | bc -l`
fi


#Make and clean tmp dirs
for d in f u p s; do
	rm -r "$tmp"/$d
	mkdir -p "$tmp"/$d
done


#Split vid into segments
ffmpeg -y -i "$in" -c copy -map 0 -segment_time $seg -f segment "$tmp/s/%05d.$fmt"

for f in "$tmp"/s/*; do
	#Split vid into frames
	ffmpeg -y -i "$f" -r $fps -vf "yadif" "$tmp/f/%05d.png"

	#Upscale frames
	waifu2x-converter-cpp -s -p 0 --noise-level $noise --scale-ratio $ratio -i "$tmp/f" -o "$tmp/u"
	#cp "$tmp/f"/* "$tmp/u"
	#ls "$tmp/u/"* | awk -F '.' '{print "mv", $0, $1 "_test" $2}' | sh

	#Rename files to make joining easier
	ls "$tmp/u/"* | awk -F _ '{print "mv", $0, $1 ".png"}' | sh

	#Join upscaled frames
	ffmpeg -y -r $fps -i "$tmp/u/%05d.png" \
		-vcodec libx264 -preset veryslow -crf 18 -profile:v high -pix_fmt yuv420p \
			-level 4.1 -sar $sar -aspect $dar \
		"$tmp"/p/`basename $f`


	#Delete the old frames and section file
	rm "$tmp"/f/*
	rm "$tmp"/u/*
	rm "$f"
done

#Join video parts and add source audio
for f in "$tmp"/p/*"$fmt"; do
	n=`basename "$f"`
	echo "file '$n'" >> "$tmp/p/list.txt"
done

ffmpeg -y -f concat -safe 0 -i "$tmp/p/list.txt" -i "$in" -map 0:v -map 1 -map -1:v -c copy "$out"

